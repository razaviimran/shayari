<?php

    session_start();

    $pageTitle = $_SERVER['REQUEST_URI'];
    if ($pageTitle == "/shayari-v4/about.php") {
        $pageTitle ="About Us";
    } else if ($pageTitle == "/shayari-v4/signin.php"){
        $pageTitle ="Signin Page";
    } else if ($pageTitle == "/shayari-v4/index.php"){
        $pageTitle ="Shayari";
    } elseif ($pageTitle == "/shayari-v4/pnp.php") {
        $pageTitle = "Privacy & Policy";
    } elseif ($pageTitle == "/shayari-v4/tnc.php") {
        $pageTitle = "Terms & Conditions";
    }

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/navbar.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/signin.css">
    <link rel="stylesheet" href="assets/css/signup.css">
    <title><?= $pageTitle ?></title>
</head>

<body>
    <header class="nav_style">
        <div class="row">
            <div class="col-md-10 col-11 mx-auto">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="index.php">
                            <img src="assets/images/shayari-logo.png" alt="" width="150" height="60">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        
                        <form class="form-inline form-navbar my-2 my-lg-0 order-2" action="">
                            <input class="form-control" name="s" type="text" placeholder="Search" />
                        </form>
                        <ul class="navbar-nav mr-auto order-1">
                            <li class="nav-item">
                                <a class="nav-link <?= $pageTitle == "Home" ? 'active':'' ?>" href="index.php">
                                    <i class="fal fa-house"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $pageTitle == "About Us" ? 'active':'' ?>" href="about.php">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $pageTitle == "Privacy & Policy" ? 'active':'' ?>" href="pnp.php">Privacy & Policy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $pageTitle == "Terms & Conditions" ? 'active':'' ?>" href="tnc.php">Terms & Conditions</a>
                            </li>
                        </ul>
                        

                        <?php 
                        // if (!isset($_SESSION)) { 
                        //     // session_start(); 
                        // }
                        if (isset($_SESSION['username'])) {
                            $username = $_SESSION['username'];
                            ?>
                            <ul class="navbar-nav d-none d-lg-flex ml-2 order-3">
                                <li class="nav-item">
                                    <a type="button" class="btn btn-outline-primary m-1" href="logout.php">Logout</a>
                                </li>
                            </ul>

                            <?php 
                        } else { ?>

                                <ul class="navbar-nav d-none d-lg-flex ml-2 order-3">
                                    <li class="nav-item">
                                        <a type="button" class="btn btn-outline-primary m-1" href="signin.php">Sign In</a>
                                    </li>
                                    <li class="nav-item">
                                        <a type="button" class="btn btn-outline-primary m-1" href="signup.php">Sign Up</a>
                                    </li>
                                </ul>

                                <?php } ?>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>