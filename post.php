<div class="card-body card border-0 shadow m-1 ">

    <div class="row ml-4 mt-1 post-bg ">
        <img src="<?= isset($_SESSION['user_image']) ? $_SESSION['user_image']: 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg' ?>" class="rounded-circle user-img">
        <h4 class="ml-2 mt-1 user-name ">
            <?= isset($_SESSION['username'])? $_SESSION['username']:"Welcome User" ?>
        </h4>
        <p class="ml-2 text-muted"><em><?= isset($_SESSION['bio']) ? $_SESSION['bio']: 'Bio' ?></em></p>
    </div>
<form id="post">
    <input class="form-control form-control-md" type="text" placeholder="Write some hilarious SHAYARI here ',' comma will break your line. " id="shayari">

    <div class="row mt-2">
        <div class="col-lg-6">
            <input id="tag1" class="form-control form-control" type="text" placeholder="tag 1">
        </div>
        <div class="col-lg-6">
            <input id="tag2" class="form-control form-control" type="text" placeholder="tag 2">
        </div>
    </div>
    <input type="hidden" name="">
    </form>
    <div class="mt-2 text-right font-weight-bold">
    	<button type="button" class="btn btn-success" onclick="AddPost()">Post</button>
    </div>
</div>

<script>

function AddPost()
{
    var content= $('#shayari').val();
    var tag1= $('#tag1').val();
    var tag2= $('#tag2').val();

             $.ajax({
            type: 'POST',
            url: 'addpost.php',
            data: {
                'content':content,
               'tag1':tag1,
                'tag2':tag2
            },  
            success:function(data) {
                    if(data == "success")
                    {
                          swal({
                    title: "Success!",
                    text: "Post Added Successfully!",
                    type: "success",
                    confirmButtonText: "Ok"
                    });
                          $("#post")[0].reset();
                    }
            },
            error:function(xhr,status,error){
               console.log(JSON.stringify(xhr));
            }
        });
}
</script>

<?php
  // include "contoller/db.php";
    
    // $db = mysqli_connect("localhost","root","","shayari");

    // if(!$db)
    // {
    //     die("Connection failed: " . mysqli_connect_error());
    // }

    // if (isset($_POST) && !empty($_POST)) {
        
    // }


?>