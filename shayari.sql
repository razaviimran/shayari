-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `shayari` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `shayari`;

DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `created_at`) VALUES
(1,	1,	1,	'2020-12-21 16:31:34'),
(3,	1,	2,	'2020-12-21 16:31:34'),
(4,	2,	1,	'2020-12-21 16:31:34');

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `bio` varchar(128) NOT NULL,
  `user_image` varchar(512) NOT NULL,
  `shayari` varchar(512) NOT NULL,
  `tag1` varchar(512) NOT NULL,
  `tag2` varchar(512) NOT NULL,
  `tag3` varchar(512) NOT NULL,
  `posted_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `post` (`id`, `username`, `bio`, `user_image`, `shayari`, `tag1`, `tag2`, `tag3`, `posted_at`) VALUES
(1,	'IMRAN',	'Part Time Shayar',	'https://scontent.fbom29-1.fna.fbcdn.net/v/t1.0-9/33525784_2077582195822667_6936275980067536896_n.jpg?_nc_cat=106&ccb=2&_nc_sid=09cbfe&_nc_ohc=T5w-pPfFHs0AX8qN2gy&_nc_ht=scontent.fbom29-1.fna&oh=a076b499d6c88b4b3a76cea671d4f821&oe=6004EC82',	'Dua to Dil se Mangi Jaati hai,<br> Zubaan se nahi ae - Iqbal,<br> Qubool to uski bhi hoti hai,<br> Jiski Zubaan nahi hoti.',	'dard',	'Allama Iqbal',	'bewafa',	'2020-12-13 14:32:15'),
(3,	'Lucky ali',	'Full Time Shayar',	'https://i.imgur.com/W8lIWnf.jpg',	'Unko dekhne se jo aa jaati hai, <br>Chehre pe rounak - Ghalib, <br>Wo samajhte hai, <br>Bimar ka haal accha hai.',	'dard',	'ghalib',	'ishq',	'2020-12-21 15:18:17'),
(4,	'Hamirdur Rahman',	'Full Time Shayar',	'https://i.imgur.com/93d4dED.jpg',	'Kitna khauf hota hai, <br>Shaam ke andhere me, <br>Poonch un parindo se, <br>Jinke ghar nahi hote.',	'hamidur',	'ehsaas',	'ishq',	'2020-12-21 15:31:05'),
(5,	'Saajid',	'Shayar',	'https://i.imgur.com/KteIIZm.jpg',	'Kalam se likh nahi sakte,<br> Hum apne DIL ke afsaane,<br> Hum to tujhe DIL se yaad karte hain,<br> Tere DIL ki khuda jaane.',	'Ghalib',	'Ishq',	'Dard',	'2021-01-10 08:09:44'),
(6,	'Imran',	'Shayari Lover',	'https://i.imgur.com/nSzSRaV.jpg',	'Kuch der ki khamoshi hai,<br>Fir Shor aayega,||2||<br>Tumhara to sirf waqt aaya hai,<br> Hamara daur aayega.',	'Sher',	'Shayari',	'Imran',	'2021-01-10 08:20:48');

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tags` (`id`, `post_id`, `name`, `description`, `created_at`) VALUES
(1,	1,	'tag1',	'tag desc',	'2020-12-21 16:24:00'),
(2,	1,	'tag2',	'tag 2 desc',	'2020-12-21 16:24:00');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bio` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `image`, `bio`, `created_at`, `updated_at`) VALUES
(1,	'gautam',	'singh',	'gautam@gmail.com',	'qwerty',	'https://i.imgur.com/O4jU9go.jpg',	'PHP Developer',	'2020-12-17 15:58:39',	'2020-12-17 15:58:39'),
(2,	'Imran',	'Shaikh',	'razaviimran@gmail.com',	'qwerty',	'https://i.imgur.com/nSzSRaV.jpg',	'PHP Developer',	'2020-12-17 15:58:46',	'2020-12-17 15:58:46'),
(3,	'Lucky',	'Ali',	'luckyali@gmail.com',	'qwerty',	'https://i.imgur.com/W8lIWnf.jpg',	'Part Time Shayar',	'2020-12-17 15:58:59',	'2020-12-17 15:58:59'),
(4,	'Waseek',	'Ansari',	'waseek@gmail.com',	'qwerty',	'https://i.imgur.com/1CLL4nU.jpg',	'Part Time Shayar',	'2020-12-17 15:59:07',	'2020-12-17 15:59:07'),
(5,	'Hamidur',	'Rahman',	'hrahman@gmail.com',	'qwerty',	'https://i.imgur.com/93d4dED.jpg',	'Full Time Shayar',	'2020-12-17 15:59:54',	'2020-12-17 15:59:54'),
(6,	'Saajid',	'Shaikh',	'sshaikh@gmail.com',	'qwerty',	'https://i.imgur.com/KteIIZm.jpg',	'Full Time Shayar',	'2020-12-17 16:00:25',	'2020-12-17 16:00:25');

-- 2021-01-10 09:02:32