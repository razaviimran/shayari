<?php
  include ("controller/db.php");
  $sql = "SELECT * FROM users";
  $result = $conn->query($sql);

  include ("header.php");
?>

<div class="container">
    <div class="row">

      <div class="col-lg-2 mt-2">
        <div class = "mt-1">
          <p class="font-weight-bold">@ TAGS</p>
        </div>
        
        <hr>
        
        <div class="mt-1">
          
          <div class="row">
           <!--  <img src="https://qph.fs.quoracdn.net/main-thumb-ti-1579247-100-sgxltnjyyzhsjxlptwtqjkvpftuudmjl.jpeg" class="mt-1 rounded-circle" style="width: 10%; height: 10%;"> -->
            <p class="mb-2 ml-1">Ghalib</p>                
          </div>
          <div class="row">
            <!-- <img src="https://qph.fs.quoracdn.net/main-thumb-ti-1577484-100-ocvtaejrkfvrhbcxrirehpyxnxowsipx.jpeg" class="mt-1 rounded-circle" style="width: 10%; height: 10%;"> -->                
            <span></span>
            <p class="mb-2 ml-1">Faraz</p>
          </div>
          <div class="row">
            <!-- <img src="https://qph.fs.quoracdn.net/main-thumb-ti-1579247-100-sgxltnjyyzhsjxlptwtqjkvpftuudmjl.jpeg" class="mt-1 rounded-circle" style="width: 10%; height: 10%;"> -->
            <p class="mb-2 ml-1">Allama Iqbal</p>                
          </div>
          <div class="row">
            <!-- <img src="https://qph.fs.quoracdn.net/main-thumb-ti-1577484-100-ocvtaejrkfvrhbcxrirehpyxnxowsipx.jpeg" class="mt-1 rounded-circle" style="width: 10%; height: 10%;"> -->                
            <span></span>
            <p class="mb-2 ml-1">Rahat Indori</p>
          </div>
        </div>
      </div>
      
      <div class="col-sm-6 mt-2">
        <?php include ('post.php'); ?>
        <?php include ('center.php'); ?>
      </div>

      <div class="col-sm-3 mt-2">
          <table class="table">
              <thead>
                  <tr>
                      <th>#</th>
                      <td class="font-weight-bold"scope="col">Most Liked Shayar</td>
                      <td><a href="">
                          <i class="fas fa-thumbs-up"></i>
                      </a></td>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Hamidur Rahman</td>
                    <td>395</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Saajid Shaikh</td>
                    <td>390</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Dilshad</td>
                    <td>385</td>
                  </tr>
                  <tr>
                    <th scope="row">4</th>
                    <td>Lucky</td>
                    <td>375</td>
                  </tr>
                  <tr>
                    <th scope="row">5</th>
                    <td>Shahid</td>
                    <td>365</td>
                  </tr>
                  <tr>
                    <th scope="row">6</th>
                    <td>Iqbal</td>
                    <td>365</td>
                  </tr>
                  <tr>
                    <th scope="row">7</th>
                    <td>Waseek</td>
                    <td>365</td>
                  </tr>
                  <tr>
                    <th scope="row">8</th>
                    <td>Gammy</td>
                    <td>365</td>
                  </tr>
              </tbody>
          </table>
      </div>

    </div>

</div>

<?php
  include("footer.php");
?>
