<?php 
include("header.php");
print_r($_SESSION);exit();
if (!isset($_SESSION['username'])) {
	header("Location:index.php");
}

?>

<div class="signup-form">
	<form action="" method="post">
		<h2>Register</h2>
		<?php
		if(!isset($_SESSION)) 
		{ 
			session_start(); 
		}
		if(!empty($_SESSION['message'])) {
			$message = $_SESSION['message'];
		}
		?>
		<p class="hint-text">Create your account. It's free and only takes a minute.</p>
		<div class="form-group">
			<div class="col-xs-6">
				<input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">
			</div>			
		</div>
		<div class="form-group">
			<div class="col-xs-6">
				<input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">
			</div>
		</div>
		<div class="form-group">
			<input type="email" class="form-control" name="email" placeholder="Email" required="required">
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="password" placeholder="Password" required="required">
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
		</div>
		<h5 style="color: red;text-align: center;">
			<?php
			if(isset($message)){
				echo $message;
				session_destroy();
			}		
			?>
		</h5>
		<div class="form-group">
			<label class="checkbox-inline"><input type="checkbox" required="required"> I accept the
				<a href="tnc.php">Terms of Use</a> &amp; 
				<a href="pnp.php">Privacy Policy</a>
			</label>
		</div>
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-success btn-lg btn-block" value="Register Now" />
		</div>
	</form>
	<div class="text-center">Already have an account? <a href="signin.php">Sign in</a></div>
</div>

<?php
include ("footer.php");
?>

<?php
if(isset($_POST['submit']))
{
	$firstName = $_POST['first_name'];
	$lastName = $_POST['last_name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$con_pass = $_POST['confirm_password'];

	include("config.php");

	if($password === $con_pass)
	{
		$sql = "INSERT into users(`first_name`,`last_name`,`email`,`password`) VALUES ('$firstName','$lastName','$email','$password')";
		$result = mysqli_query($conn,$sql);
		if($result)	{	
			echo "<script src'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>";
			echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
			echo "<script>swal('Successfully Register!');</script>";
		} else {
			include("error.php");
		}
	} else {
		session_start();
		$_SESSION['message'] = "Password does not match";
		header("Location: signup.php");
	}
}	
?>
