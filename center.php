<?php 
include("controller/db.php");

$sql = "SELECT * FROM posts JOIN users ON users.id = posts.user_id ORDER BY posts.id DESC";
$result = $conn->query($sql);
?>


<?php if($result->num_rows > 0): ?>
    
    <?php while ($row = $result->fetch_assoc()): ?>
        <div class="card-body post-bg custom-rad card border-0 shadow mt-2">
            <div class="row mt-2 ml-2">
                <img src="<?= $row['image']; ?>" class="user-img rounded-circle">
                <h4 class="ml-4 mt-1 user-name"><?= $row['first_name']; ?></h4>
                <p class="ml-3 text-muted"><em><?= $row['bio'] ?></em></p>
            </div>
            <hr>
            <p class="card-text text-center">
                <?= $row['shayari']; ?>
            </p>
            <div class="card-body">
                <footer class="blockquote-footer">Posted At <?= date("Y M d H:i:s",strtotime($row['created_at'])); ?> 
                        <span class="badge badge-primary ml-3"><?= $row['tag1'] ?></span>
                        <span class="badge badge-primary ml-3"><?= $row['tag2'] ?></span>
                    </footer>      
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <i class="fab fa-thumbs-up mr-3"></i>
                    </div>
                    <div class="col-md-4">
                        <i class="fab fa-thumbs-down mr-3"></i>
                    </div>
                    <div class="col-md-4">
                        <i class="fab fa-comments mr-3"></i>
                    </div>
                </div>  
            </div>
        </div>
    <?php endwhile; ?>
    
<?php endif; ?>
