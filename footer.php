    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">
        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
            <a href="" target="_blank">
                <i class="fab fa-facebook-f mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-twitter mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-youtube mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-pinterest mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-github mr-3"></i>
            </a>
        </div>
        <div class="footer-copyright py-3">
            © 2019 Copyright:
            <a href="https://razaviimran.blogspot.com/" target="_blank"> Su-Im </a>
        </div>
    </footer>

<!-- JS, Popper.js, and jQuery -->
<!-- <script src="assets/js/jquery-3.5.1.slim.min.js"></script> -->
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>

</body>

</html>