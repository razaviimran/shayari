<?php

include("header.php");
include("controller/db.php");

// if (isset($_SESSION['username'])) {
//   header("Location:index.php");
// }
?>
  <div class="body">
    <form class="form-signin" action="" method="POST">
      <h1 class="h3 mb-3 font-weight-normal">Log In to Shayari</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign in</button>
    </form>
  </div>

<?php
if(isset($_POST['submit']))
    {
        
        $email = $_POST['email'];
        $password = $_POST['password'];

        $sql = "SELECT * from users where email='$email'";
      
        $result = mysqli_query($conn,$sql);

        $row = mysqli_num_rows($result);
        
        if($row > 0) {
          $email_result = mysqli_fetch_array($result);

          if($email_result['password'] == $password) { 
              $_SESSION['id'] = $email_result['id'];
              $_SESSION['username'] = $email_result['first_name'];
              $_SESSION['user_image'] = $email_result['image'];
              $_SESSION['bio'] = $email_result['bio'];
              header("Location:index.php");
          } else {
              echo "<script src'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>";
              echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
              echo "<script>swal('Invalid Password!');</script>";
          }
        } else {
              echo "<script src'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>";
              echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
              echo "<script>swal('Invalid Email!');</script>";
        }
    }

include("footer.php");
?>
